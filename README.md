> What is this?
 - A demonstration of a linux host running in Azure, created by using Terraform available for further configuration via a dynamic ansible inventory.

> No really, what is this.
Well there's sorta two things going on here. There's the Terraform bits and the Ansible bits.
 1. `main.tf` is the Terraform stuff. It uses the azure-cli in your shell to interact with the Azure API and create stuff in a relatively easy to understand configuration language.
 1. `ansible.cfg`, `inventory/*.yaml` and `ssh_config` is the ansible magick.
    The `ansible.cfg` says look in `inventory/` for your inventory. In there is `inventory/inventory_azure_rm.yaml` which configures ansibles Azure Resource Manager dynamic inventory system. The `ssh_config` enables using a bastion host for machines w/o a public IP. (aside: `man 1 ssh-keyscan`).

 > Can you break it down some?
 Certainly. The terraform file is heavily commented. The ansible setup just a regular old ansible.cfg + ssh config with some templating to fill in bits after the terraform run.

 ## Terraform bits
 Reference [main.tf](main.tf) as each section is commented

 ## Ansible bits
 Reference [inventory](inventory/inventory_azure_rm.yaml) [ansible.cfg](templates/ansible.cfg.tpl) and the [ssh_config](templates/ssh_config.tpl)

 ### Extra Credit
  - `ansible-inventory --list`
    - add one of the azure tags as an ansible group?
