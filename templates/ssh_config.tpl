Host ${private-subnet}
  StrictHostKeyChecking no
  ProxyCommand ssh -W %h:%p -i ${pubkey} ${bastion-user}@${bastion-fqdn}
  User ${node-admin}
  IdentityFile ${pubkey}

Host ${bastion-fqdn}
  User ${bastion-user}
  StrictHostKeyChecking no
  IdentityFile ${pubkey}
  ControlMaster auto
  ControlPath ~/.ssh/ansible-%r@%h:%p
  ControlPersist 5m