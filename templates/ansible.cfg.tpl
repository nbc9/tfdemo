[defaults]
enable_plugins = azure_rm
inventory = ./inventory
host_key_checking = False
output_plugin = unixy
ansible_become = True
[ssh_connection]
ssh_args = -F ./${ssh_config} -o ControlMaster=auto -o ControlPersist=30m
control_path = ~/.ssh/ansible-%%r@%%h:%%p
