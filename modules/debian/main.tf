# compute node resources
variable "debian-count" {
  default = 1
}

variable "debian-machine-type" {}

variable "subnet" {}

variable rg {}

variable prefix {}

variable ssh-key {}

resource "azurerm_network_interface" "nic" {
  count                   = "${var.debian-count}"
  name                    = "${var.prefix}-debian-${count.index}-nic"
  location                = "${var.rg.location}"
  resource_group_name     = "${var.rg.name}"
  internal_dns_name_label = "debian-${count.index}"

  ip_configuration {
    name                          = "private-ip"
    subnet_id                     = "${var.subnet.id}"
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_virtual_machine" "debian" {
  count                            = "${var.debian-count}"
  name                             = "${var.prefix}-node-${count.index}"
  location                         = "${var.rg.location}"
  resource_group_name              = "${var.rg.name}"
  network_interface_ids            = ["${element(azurerm_network_interface.nic.*.id, count.index)}"]
  vm_size                          = "${var.debian-machine-type}"
  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    offer     = "debian-10"
    publisher = "Debian"
    sku       = "10"
    version   = "latest"
  }
  storage_os_disk {
    name              = "debian-disk-${count.index}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    disk_size_gb      = "30"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    admin_username = "debian"
    computer_name  = "debian-${count.index}"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      key_data = "${var.ssh-key}"
      path     = "/home/debian/.ssh/authorized_keys"
    }
  }
  tags = {
    environment = "${var.prefix}"
    node-role   = "compute"
  }
}
