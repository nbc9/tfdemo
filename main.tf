/*
* the provider and version to use for this terraform setup
*/
provider "azurerm" {
  version = "~> 1.33"
}

/*
* location:
*   see: https://azure.microsoft.com/en-us/global-infrastructure/locations/
*/
variable "location" {
  default     = "eastus"
  description = "Resource location"
}

/*
* prefix:
*   a bit of string that gets prefixed to our resource names. it's merely a convention.
*/
variable "prefix" {
  default     = "linux-demo"
  description = "prefix for resource names"
}

/*
* machine-type:
*   an identifier of what spec of VM to build see https://docs.microsoft.com/en-us/azure/virtual-machines/linux/sizes-general
*/
variable "machine-type" {

  default     = "Standard_B1s"
  description = "1vCPU 1GiB Memory"
}

/*
* ssh-public-key:
*   This is my public key. obviously in real life we'd use ${something} to populate the desired keys.
*   The magic of the cloud will put this on the system when it's booted.
*/
variable "ssh-public-key" {
  description = "nate's public ssh key"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC8FSGoIO6NS+hrA0l9Qv+WUggOaQ+LpWdu6Fw5YUeZD8fskp9CO+uu6k5H1/efqy0y1OVPhAI18wuMDkh2krAX5iVRHEva++/N7oTp7NG4TkElOFkfAgAQlqQvrSi84j9tENbmPzpu54b20rSi3QpoEiV1bTZSZfhqTBN9QEgWAfdo6XMT/RGDo72hrxoFoLIiRhEfwU8vq+UiRjvkESBJU6mIpUlZ+bC2Bcu98eVSXWtbCdoUXoU0/CyqFky7HYepIwhkbZr5CTzEqVGkjii353SgKJJgYREfr0jgPJkd1BS7wzbR8BB8FHV7LwIWIiENb8aaYFJb+UpJnWllMFHzUl9rf/fVc/RdvHjVNC6WRTCjaJhOlWHDB9vjRPH3j7u9vZavB8wsiAQWo8/oopo4u0s6QSMlX/UuZBcni6XfbsyXU/7cFxsyXzTKXMyIQDAXNdS5DmBvsHpEKMexfn25+PmqWRW6QD4N9EWz7WRJq+1QQF7O8zPaWiBzsXVPicm2ni9OG4VdG0KTRdIy9oJihnYXv4Mf7JSTOHWtF/fFgqs2A2pc7U+HfeegX+6hPDJ4lBWBmfNbLZAcqUdgIH7nL9OXGB6TJvY3OOkBjB15pXqrAn/jL2rc2p3wPjbTWmYi5C0zC4kun1Lo+Chf42JenL0oMXGM1Jri4LOueHILSQ== nate@askew"
}

/*
* bastion-admin-username:
*   The ${USERNAME} of the user that is present on the system. Hint: it's atomic on atomic, centos on centos and ubuntu on ubuntu.
*   See your OS vendor's documentation on their cloud images for specifics.
*/
variable "bastion-admin-username" {
  default     = "ubuntu"
  description = "default username on the bastion vm"
}

variable "node-admin-username" {
  default     = "debian"
  description = "default username for node vm"
}
/*
* Resource group to hold all the resources.
*/
resource "azurerm_resource_group" "rg" {
  name     = "${var.prefix}"
  location = "${var.location}"
  tags = {
    Project = "${var.prefix}"
  }
}

/*
* virtual network named "${PREFIX}-network"
*/
resource "azurerm_virtual_network" "main" {
  name                = "${var.prefix}-network"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  location            = "${azurerm_resource_group.rg.location}"
  address_space       = ["10.8.0.0/24"]
}

/*
* subnet that refers to our "main" network
*/
resource "azurerm_subnet" "internal" {
  name                 = "${var.prefix}-internal-subnet"
  resource_group_name  = "${azurerm_resource_group.rg.name}"
  virtual_network_name = "${azurerm_virtual_network.main.name}"
  address_prefix       = "${azurerm_virtual_network.main.address_space[0]}"
}
/*
* public ip for our bastion host
*/
resource "azurerm_public_ip" "pip" {
  name                    = "${var.prefix}-pip"
  resource_group_name     = "${azurerm_resource_group.rg.name}"
  location                = "${azurerm_resource_group.rg.location}"
  allocation_method       = "Dynamic"
  idle_timeout_in_minutes = 30
  domain_name_label       = "${var.prefix}-bastion"
}
/*
* network interface attached to our subnet and mapped to our public IP
*/
resource "azurerm_network_interface" "nic" {
  name                = "${var.prefix}-nic"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  location            = "${azurerm_resource_group.rg.location}"

  ip_configuration {
    name                          = "with-public"
    subnet_id                     = "${azurerm_subnet.internal.id}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = "${azurerm_public_ip.pip.id}"
  }
}

/*
* virtual machine using our network interface with our key crammed into the bastion users' authorized_keys
*/
resource "azurerm_virtual_machine" "bastion" {
  name                = "${var.prefix}-bastion"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  location            = "${azurerm_resource_group.rg.location}"

  vm_size                          = "${var.machine-type}"
  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true
  network_interface_ids            = ["${azurerm_network_interface.nic.id}"]
  storage_image_reference {
    offer     = "UbuntuServer"
    publisher = "Canonical"
    sku       = "18.04-LTS"
    version   = "latest"
  }
  storage_os_disk {
    name              = "bastion-os-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    admin_username = "${var.bastion-admin-username}"
    computer_name  = "bastion"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      key_data = "${var.ssh-public-key}"
      path     = "/home/${var.bastion-admin-username}/.ssh/authorized_keys"
    }
  }
}
/*
* this is a terraform module that will create an arbitrary number of backend
* systems that we'll connect to via the bastion host  we created above.
*/
module "debian-machines" {
  source              = "./modules/debian"
  rg                  = "${azurerm_resource_group.rg}"
  debian-count        = 2
  debian-machine-type = "${var.machine-type}"
  prefix              = "${var.prefix}"
  subnet              = "${azurerm_subnet.internal}"
  ssh-key             = "${var.ssh-public-key}"
}
/*
* Output the public FQDN for the bastion host
*/
output "bastion-fqdn" {
  value       = "${azurerm_public_ip.pip.fqdn}"
  description = "FQDN of bastion host"
}
/*
* data resource to assemble our ssh_config
*/
data "template_file" "ssh_config" {
  template = "${file("templates/ssh_config.tpl")}"
  vars = {
    private-subnet = "10.8.*.*"
    bastion-user   = "${var.bastion-admin-username}"
    bastion-fqdn   = "${azurerm_public_ip.pip.fqdn}"
    node-admin     = "${var.node-admin-username}"
    pubkey         = "${local_file.pubkey.filename}"
  }
}

/*
* data resource to assemble our public key(s?)
*/
data "template_file" "pubkey" {
  template = "${file("templates/pubkey.tpl")}"
  vars = {
    pubkey = "${var.ssh-public-key}"
  }
}
/*
* data resource to generate ansible.cfg
*/
data "template_file" "ansible_cfg" {
  template = "${file("templates/ansible.cfg.tpl")}"
  vars = {
    ssh_config = "${local_file.ssh_config.filename}"
  }
}

/*
* local file resource that is the ssh_config (for ansible)
*/
resource "local_file" "ssh_config" {
  content  = "${data.template_file.ssh_config.rendered}"
  filename = "ssh_config"
}
/*
* local file resource that contains our public key
*/
resource "local_file" "pubkey" {
  content  = "${data.template_file.pubkey.rendered}"
  filename = "pubkey.pub"
}

/*
* local file resource that contains the ansible configuration
*/
resource "local_file" "ansible_cfg" {
  content  = "${data.template_file.ansible_cfg.rendered}"
  filename = "ansible.cfg"
}
